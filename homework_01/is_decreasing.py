def is_decreasing(number_list):
    for index in range(0, len(number_list) - 1):
        if number_list[index] <= number_list[index + 1]:
            return False
    return True


print(is_decreasing([5, 4, 3, 2, 1]))
print(is_decreasing([1, 2, 3]))
print(is_decreasing([100, 50, 20]))
print(is_decreasing([1, 1, 1, 1]))
