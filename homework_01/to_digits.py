def to_digits(number):
    digit_list = []
    while number > 0:
        digit = number % 10
        digit_list.append(digit)
        number = number // 10
    digit_list.reverse()
    return digit_list


print(to_digits(123))
print(to_digits(999999))
print(to_digits(123023))
