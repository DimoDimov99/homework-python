def fib_number(number):
    current_number = 1
    previous_number = 0
    fib_num_arr = [1]
    fib_nums_sum = 0

    for num in range(1, number):
        next_number = current_number + previous_number
        fib_num_arr.append(next_number)
        previous_number = current_number
        current_number = next_number
    for num in fib_num_arr:
        if num <= 10:
            fib_nums_sum = fib_nums_sum * 10
        else:
            fib_nums_sum = fib_nums_sum * 100
        fib_nums_sum += num
    return fib_nums_sum


print(fib_number(5))