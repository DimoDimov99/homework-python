def sum_of_digits(number):
    if number < 0:
        number = abs(number)
    sum = 0
    while number > 0:
        digit = number % 10
        sum += digit
        number = number // 10
    return sum

print(sum_of_digits(1325132435356))
print(sum_of_digits(123))
print(sum_of_digits(6))
print(sum_of_digits(-10))