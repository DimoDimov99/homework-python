def is_number_balanced(number):
    number_digits = []
    left_part = 0
    right_part = 0
    while number % 10 > 0:
        digit = number % 10
        number_digits.append(digit)
        number = number // 10
    number_digits.reverse()
    list_lenght = len(number_digits)
    middle = len(number_digits) // 2
    
    if len(number_digits) == 1:
        return True

    for index in range(list_lenght):
        if index < middle:
            left_part = left_part + number_digits[index]
        else:
            right_part = right_part + number_digits[index]
    return left_part == right_part
    
    
print(is_number_balanced(9))
print(is_number_balanced(4518))
print(is_number_balanced(28471))
print(is_number_balanced(1238033))