def to_number(digits):
    number_sum = 0
    for digit in digits:
        if digit <= 10:
            number_sum = number_sum * 10
        else:
            number_sum = number_sum * 100
        number_sum += digit
    return number_sum

print(to_number([1, 2, 3]))
print(to_number([9,9,9,9,9,9]))
print(to_number([1, 2, 3, 0, 2, 3]))
print(to_number([21, 2, 33]))

