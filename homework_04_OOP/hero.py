import random


class Entity():
    def __init__(self, name, health):
        self._name = name
        self._health = health
        self._damage = 0
        self._weapon = None

    def get_health(self):
        return self._health

    def is_alive(self):
        if self._health > 0:
            return True
        return False

    def take_damage(self, damage_points):
        current_hero_health = self._health
        reduced_health = current_hero_health - damage_points
        if reduced_health < 0:
            reduced_health = 0
        self._health = reduced_health
        return reduced_health

    def take_healing(self, healing_points):
        if self._health <= 0:
            return False
        if self._health >= 100:
            return "The hero health is already at maximum!"
        else:
            if self._health + healing_points <= 100:
                self._health += healing_points
                return True
            if self._health + healing_points > 100:
                return "We cannot heal our hero above the maximum health"

    def has_weapon(self):
        if self._weapon is not None:
            return True
        return False

    def equip_weapon(self, weapon):
        self._weapon = weapon

    def attack(self):
        if self._weapon is not None:
            if self._weapon.is_critical_hit():
                return self._weapon._damage
            else:
                return self._weapon._damage

        else:
            return 0


class Weapon(Entity):
    def __init__(self, type, damage, critical_strike_percent):
        self._type = type
        self._damage = damage
        self._critical_strike_percent = critical_strike_percent

    def critical_hit(self, critical_strike_percent):
        return random.random() < self._critical_strike_percent


class Hero(Entity):
    def __init__(self, name, health, nickname):
        self._nickname = nickname
        super().__init__(name, health)

    def __str__(self):
        return f"{self._name} The {self._nickname}"


class Orc(Entity):
    def __init__(self, name, health, berserk_factor):
        self._berserk_factor = berserk_factor
        self.goes_berserk(berserk_factor)
        super().__init__(name, health)

    def __str__(self):
        return f"The mighty {self._name}"

    def goes_berserk(self, berserk_factor):
        if berserk_factor < 1:
            berserk_factor = 1
        elif berserk_factor > 2:
            berserk_factor = 2
        self._berserk_factor = berserk_factor

    def attack(self):
        return Entity.attack(self) * self._berserk_factor


myhero = Hero("Bron", 100, "DragonSlayer")
myorc = Orc("Milko Kalaidjiev", 100, 2)
myweapon = Weapon("Mighty Axe", 25, 0.2)

print(myhero)
print(myhero.get_health())
print(myhero.is_alive())
print(myhero.take_damage(61))
print(myhero._health)
print(myhero.take_healing(51))
print(myhero._health)
print(myorc)
print(myhero.equip_weapon("Axe"))
print(myhero.has_weapon())
