def is_prime(number):
    if number <= 1:
        return False
    # if number is only divisible by 1 and itself it is prime
    # 1 is not prime so we start from 2!
    for num in range(2, number):
        if number % num == 0:
            return False
    return True


print(is_prime(1))
print(is_prime(2))
print(is_prime(8))
print(is_prime(11))
print(is_prime(-10))