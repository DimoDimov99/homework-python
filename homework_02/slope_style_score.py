import math
def truncate(number, digits):
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper

def slope_style_score(scores):
    min = scores[0]
    max = scores[0]
    nums_sum = 0

    for item in scores:
        if item < min:
            min = item
        if item > max:
            max = item
    scores.remove(min)
    scores.remove(max)

    for num in scores:
        nums_sum = nums_sum + num
    avg_list = nums_sum / len(scores)
    avg_list_formatted = truncate(avg_list, 2)
    return avg_list_formatted 


print(slope_style_score([94, 95, 95, 95, 90]))
print(slope_style_score([60, 70, 80, 90, 100]))
print(slope_style_score([96, 95.5, 93, 89, 92]))