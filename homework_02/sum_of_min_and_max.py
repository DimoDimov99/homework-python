def sum_of_min_and_max(numbers_list):
    min = numbers_list[0]
    max = numbers_list[0]
    min_max_sum = 0

    for num in numbers_list:
        if min < num:
            min = num
        if max > num:
            max = num
    min_max_sum = min + max
    return min_max_sum

print(sum_of_min_and_max([1, 2, 3, 4, 5, 6, 7, 8, 9]))
print(sum_of_min_and_max([-10, 5, 10, 100]))
print(sum_of_min_and_max([1]))