def is_int_palindrome(number):
    number_list = []
    number_list_reversed = []
    while number > 0:
        digit = number % 10
        number_list.append(digit)
        number_list_reversed.append(digit)
        number = number // 10
    number_list.reverse()
    return number_list == number_list_reversed


print(is_int_palindrome(1))
print(is_int_palindrome(42))
print(is_int_palindrome(100001))
print(is_int_palindrome(999))
print(is_int_palindrome(123))

