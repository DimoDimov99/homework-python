def contains_digit(number, digit):
    digits_list = []
    while number > 0:
        num_digit = number % 10
        digits_list.append(num_digit)
        number = number // 10
    digits_list.reverse()
    if digit in digits_list:
        return True
    return False

print(contains_digit(123, 4))
print(contains_digit(42, 4))
print(contains_digit(1000, 0))
print(contains_digit(12346789, 5))
    
