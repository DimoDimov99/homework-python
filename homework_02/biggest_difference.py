def biggest_difference(number_list):
    min_value = number_list[0]
    max_value = number_list[0]

    for item in number_list:
        if item < min_value:
            min_value = item
        if item > max_value:
            max_value = item
    difference = min_value - max_value
    return difference


print(biggest_difference([1, 2]))
print(biggest_difference([1, 2, 3, 4, 5]))
print(biggest_difference([-10, -9, -1]))
print(biggest_difference(range(100)))