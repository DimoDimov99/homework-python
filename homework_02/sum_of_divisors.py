def sum_of_divisors(number):
    divisors_sum = 0
    for num in range(1, number + 1):
        if number % num == 0:
            divisors_sum += num
    return divisors_sum




print(sum_of_divisors(8))
print(sum_of_divisors(7))
print(sum_of_divisors(1))
print(sum_of_divisors(1000))
