def count_substrings(haystack, needle):
    counter = 0
    needle_index = 0
    haystack_len = len(haystack)
    needle_len = len(needle)
    for index in range(haystack_len):
        # print(haystack[index], needle[needle_index])
        # print(50*"-")
        if haystack[index] == needle[needle_index]:
            needle_index += 1
            # print(needle[needle_index - 1])
        if needle_index == needle_len:
            needle_index = 0
            counter += 1
    return counter



print(count_substrings("This", "is"))
# print(count_substrings("babababa", "baba"))
# print(count_substrings("Python is an awesome language to program in!", "o"))
# print(count_substrings("we have nothing in common", "really?"))
# print(count_substrings("This is this and that is this", "this"))