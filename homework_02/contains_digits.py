def contains_digits(number, digits):
    digits_list = []

    while number > 0:
        digit = number % 10
        digits_list.append(digit)
        number = number // 10
    digits_list = digits_list[::-1]

    for dig in range(len(digits)):
        if digits[dig] not in digits_list:
            return False
    return True

        



print(contains_digits(402123, [0, 3, 4]))
print(contains_digits(402123, [6, 4]))
print(contains_digits(123456789, [1, 2, 3 , 0]))
print(contains_digits(456, []))