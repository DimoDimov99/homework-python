from os import read


def read_file(file):
    with open(file, "r") as reading_file:  
        file = reading_file.readline()
        while file != "":
            print(file, end="")
            file = reading_file.readline()
        return file

print(read_file("example.txt"))