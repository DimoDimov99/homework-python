def write_args_to_file():
    import sys
    args_given = sys.argv[1::]
    with open("args.txt", "a") as readed_file:
        for item in args_given:
            readed_file.write(item + "\n")
            readed_file.write("")


if __name__ == "__main__":
    write_args_to_file()

