def sum_of_numbers(st):
    placeholder_st = "0"
    number_sum = 0

    for i in st:
        if i.isdigit():
            placeholder_st += i
        else:
            number_sum += int(placeholder_st)
            placeholder_st = "0"
    return number_sum + int(placeholder_st)


print(sum_of_numbers("ab125cd3"))
print(sum_of_numbers("ab12"))
print(sum_of_numbers("ab"))