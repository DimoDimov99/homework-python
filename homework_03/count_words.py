def count_words(arr):
    dict_of_counted_words = {}
    for item in arr:
        counter = 0
        for same_item in arr:
            if item == same_item:
                counter += 1
        dict_of_counted_words[item] = counter
    return dict_of_counted_words


print(count_words(["apple", "banana", "apple", "pie"]))