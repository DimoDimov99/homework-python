def nan_expand(num):
    if num == 0:
        return '""'
    else:
        return f"{'Not a ' * num}NaN"

print(nan_expand(0))
print(nan_expand(1))
print(nan_expand(2))
print(nan_expand(3))
print(nan_expand(4))
print(nan_expand(5))