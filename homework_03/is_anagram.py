def is_anagram(fst_word, snd_word):
    fst_word = fst_word.lower()
    snd_word = snd_word.lower()
    fst_word_lenght = len(fst_word)
    snd_word_lenght = len(snd_word)

    if fst_word_lenght == snd_word_lenght:
        for item in range(fst_word_lenght):
            for item2 in range(snd_word_lenght):
                if fst_word[item] == snd_word[item2]:
                    return True
    return False


print(is_anagram("BRADE", "BeaRD"))
print(is_anagram("TOP_CODER", "COTO_PRODE"))
print(is_anagram("Tar", "Rat"))
print(is_anagram("Arc", "Car"))
print(is_anagram("test", "treserio"))